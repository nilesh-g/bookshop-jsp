CREATE TABLE CUSTOMERS(ID INT AUTO_INCREMENT PRIMARY KEY, Name VARCHAR(30),Password VARCHAR(15),Mobile VARCHAR(15),Address VARCHAR(60),Email VARCHAR(40), CONSTRAINT EMAIL_UNIQUE UNIQUE (Email));

CREATE TABLE ITEMS(ID INT AUTO_INCREMENT PRIMARY KEY, Category VARCHAR(50), Name VARCHAR(80), Info VARCHAR(150), Price DOUBLE); 

INSERT INTO CUSTOMERS VALUES(1,'admin','admin','1111111111','Online Food,Pune','admin@sunbeaminfo.com');
INSERT INTO CUSTOMERS VALUES(2,'nilesh','nilesh','9527331338','Katraj,Pune','nilesh@sunbeaminfo.com');
INSERT INTO CUSTOMERS VALUES(3,'prashant','prashant','9881208114','Katraj,Pune','prashant@sunbeaminfo.com');
INSERT INTO CUSTOMERS VALUES(4,'nitin','nitin','9881208115','ChavanNagar,Pune','nitin@sunbeaminfo.com');

INSERT INTO ITEMS(ID, Category, Name, Info, Price) VALUES
(1, 'Idli Vada', 'Steamed Idli', '', 58),
(2, 'Idli Vada', 'Idli with Butter', '', 86),
(3, 'Idli Vada', 'Dahi Idli', '', 86),
(4, 'Idli Vada', 'Fried Idli', '', 77),
(5, 'Idli Vada', 'Idli Vada', '', 67),
(6, 'Idli Vada', 'Medu Vada', '', 67),
(7, 'Idli Vada', 'Potato Vada', '', 62),
(8, 'Idli Vada', 'Dahi Vada', '', 91),
(9, 'Idli Vada', 'Sabudana Vada', '', 67),
(10, 'Dosa', 'Onion Chilli Set Dosa', '', 96),
(11, 'Dosa', 'Mysore Masala Dosa', '', 124),
(12, 'Dosa', 'Ghee Masala Dosa', '', 115),
(13, 'Dosa', 'Ghee Sada Dosa', '', 105),
(14, 'Dosa', 'Plain Dosa', '', 72),
(15, 'Dosa', 'Masala Dosa', '', 81),
(16, 'Dosa', 'Butter Plain Dosa', '', 105),
(17, 'Dosa', 'Butter Masala Dosa', '', 115),
(18, 'Dosa', 'Cheese Masala Dosa', '', 124),
(19, 'Dosa', 'Mysore Cheese Masala Dosa', '', 162),
(20, 'Uttapam', 'Plain Uttapam', '', 77),
(21, 'Uttapam', 'Plain Uttapam with Butter', '', 96),
(22, 'Uttapam', 'Onion Uttapam', '', 86),
(23, 'Uttapam', 'Onion Chilli Uttapam', '', 86),
(24, 'Uttapam', 'Butter Onion Uttapam', '', 115),
(25, 'Uttapam', 'Butter Tomato Uttapam', '', 115),
(26, 'Uttapam', 'Onion Tomato Cheese Uttapam', '', 143),
(27, 'Uttapam', 'Cheese Onion Uttapam', '', 124),
(28, 'Miscellaneous', 'Upma', '', 48),
(29, 'Miscellaneous', 'Thalipith', '', 86);


